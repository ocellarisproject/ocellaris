First paper on Ocellaris has been published
================================================

.. feed-entry::
    :author: Tormod Landet
    :date: 2019-10-20

The first paper :footcite:`landet_slope_lim` on Ocellaris has between accepted for publication in Computers & Fluids.
The paper describes the slope limiter used to stabilise the velocity field with regards to the instabilities that occur near the jump in density between the two phases.
The paper describes the limiter and shows the applicability of the method to simulate air/water flows in 2D.

The paper is available online at `DOI 10.1016/j.compfluid.2019.104322 <https://doi.org/10.1016/j.compfluid.2019.104322>`_, though it may take some days before the pre-print version is replaced with the final version.

.. figure:: dg_bdm_slope_limiter.svg
    :width: 25%
    :align: center
    :alt: Slope-limiting procedure diagram

    The slope-limiting procedure for the velocities (figure from the paper). The gray boxes indicate divergence-free fields, while the green indicate stable (slope limited) fields.
