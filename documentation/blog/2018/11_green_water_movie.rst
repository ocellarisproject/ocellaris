3D dam breaking video
=====================

.. feed-entry::
    :author: Tormod Landet
    :date: 2018-11-30

A short video showing the results from running Ocellaris on the green water
3D dam breaking benchmark by Kleefsman et al. (2005) :footcite:`kleefsman2005` has
been uploaded to `YouTube <https://www.youtube.com/watch?v=wuueXeeZ84I>`_ and
is also embedded below.

..  youtube:: wuueXeeZ84I
    :width: 80%

The input files, results, and plotting scripts can be found on Zenodo_. A paper
describing the simulation is also available :footcite:`landet_3D_wave`.

.. _Zenodo: https://zenodo.org/record/2587038
